﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JournalLabs.API.Models
{
    public class Group
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}