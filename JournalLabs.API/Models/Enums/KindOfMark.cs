﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JournalLabs.API.Models.Enums
{
    public enum KindOfMark
    {
        FirstMark,
        SecondMark
    }
}