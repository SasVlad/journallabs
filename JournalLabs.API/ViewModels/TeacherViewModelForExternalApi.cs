﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JournalLabs.API.ViewModels
{
    public class TeacherViewModelForExternalApi
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
    }
}